

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import mumbai.restClient2.RestClient;

import org.json.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList; 

public class RestTestExample 
{
	private RestClient restClient = new RestClient("http://02267-mumbai.compute.dtu.dk:8080/");
	
	// -- Scenario: Test connection
	private String testURI;
	private Response testResponse;
	
	@Given("^The service with a test URI$")
	public void theServiceWithATestURI() throws Throwable {
	    // The address is just /token/test
		testURI = "tokens/test";
	}

	@When("^Sending a GET request to the test address$")
	public void sendingAGETRequestToTheTestAddress() throws Throwable {
		
		testResponse = restClient.get(testURI, MediaType.TEXT_PLAIN);
	}

	@Then("^Receives String test$")
	public void receivesStringTest() throws Throwable {
	    assertEquals( "test", testResponse.readEntity(String.class) );
	}
	
	// -- Scenario: Request X tokens
	private long tokenAmount; 
	private Response tokenResponse;
	
	@Given("^The desired token amount (\\d+) and valid user \"([^\"]*)\" which has no tokens$")
	public void theDesiredAmountAndValidUserWhichHasTokens(int arg1, String arg2) throws Throwable {
		tokenAmount = arg1;
	}
	
	@When("^Requesting tokens through POST$")
	public void requestingTokensThroughPOST() throws Throwable 
	{
		JSONObject jObject = new JSONObject();
		jObject.put("tokenAmount", tokenAmount);
		tokenResponse = restClient.post("tokens/generator", jObject.toString(), MediaType.APPLICATION_JSON);
	}

	@Then("^receives previous amount of tokens$")
	public void receivesToken() throws Throwable {
//		System.out.println(testResponse.readEntity(String.class));
		assertEquals(tokenAmount, tokenResponse.readEntity(ArrayList.class).size());
	}
	
	// -- Scenario: Consume unused token
}
