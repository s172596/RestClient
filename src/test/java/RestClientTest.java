import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tools.Path;

public class RestClientTest 
{
	String path;
	@Test
	public void testRootPath()
	{
		path = "http://02267-mumbai.compute.dtu.dk:8080/";
		Path rootPath = new Path(path);
		assertEquals(path, rootPath.getPath());
	}
	
	@Test
	public void testRootPathAdd()
	{
		path = "http://02267-mumbai.compute.dtu.dk:8080/";
		Path rootPath = new Path(path);
		String addedPath = "tokens/test";
		assertEquals(path + addedPath, rootPath.addPath(addedPath));
	
	}
}
