Feature: Rest client features
  
  Scenario: Test connection
    Given The service with a test URI
    When Sending a GET request to the test address
    Then Receives String test

  Scenario: Create new user
    Given A name for the user "name"
    When Requesting to create a new user through POST AND this name is not in the DB
    Then Receives success
    
  Scenario: Request 1 token
    Given The desired token amount 1 and valid user "somebody" which has no tokens
    When Requesting tokens through POST
    Then Receives previous amount of tokens
    
  Scenario: Consume unused token
    Given An unused token 1
    When Request to consume token through POST
    Then Receives success the sent token is used in this client